#lang scribble/manual
@require[@for-label[csrmesh
                    racket/base]]

@title{csrmesh}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{Introduction}


@section{Protocol Documentation}

The following is a quoted description of the protocol as described by Nash Kaminski.
See https://kaminski.io  Full credit to Mr. Kaminski for his work in explaining the details of creating a CSRMesh packet.

@subsection{Network Key}

"The 128 bit network key used in CSRMesh networks is derived by concatenating the ASCII representation of the PIN with a null byte and the string 'MCP', computing the SHA256 hash of the string, reversing the order of the bytes in the resulting hash, and taking the first 16 bytes as the key."

@subsection{Authenticated Packets}

"Packets sent to CSRMesh devices require authentication as well as encryption. All multibyte types are represented in little endian format. To form a valid packet, the sequence/nonce value, constant 0x0080, and 10 null bytes are concatenated together to form a 128 bit initialization vector (IV). This IV, as well as the network key derived earlier is then used to initialize AES-128 in OFB mode. The arbitrary length data payload is then encrypted using this AES-OFB instance to form the encrypted payload. Next, a message authentication code is computed using HMAC-SHA256, using the network key as the secret, of the following data: 8 null bytes, sequence number, constant 0x80 and encrypted payload. The order of the bytes in the resulting hash are then reversed and the hash truncated to 8 bytes. The final output packet can then be built by contatenating the sequence/nonce value, constant 0x80, encrypted payload, truncated HMAC, and the constant 0xff."

@section{API}

@defmodule[csrmesh]

@subsection{Types}

@defthing[Pin bytes? #:value (pin bytes?)]{
A @racket[(define-new-subtype Pin (pin Bytes))] that represents a 4-digit ascii pin.
}

@defthing[NetKey bytes? #:value (netkey bytes?)]{
A @racket[(define-new-subtype NetKey (netkey Bytes))] that represents a CSRMesh Netkey.
}

@defthing[Encrypted-Payload bytes? #:value (encrypted-payload bytes?)]{
A @racket[(define-new-subtype Encrypted-Payload (encrypted-payload Bytes))] that represents an encrypted CSRMesh payload.
}

@defthing[Payload bytes? #:value (payload bytes?)]{
A @racket[(define-new-subtype Payload (payload Bytes))] that represents a payload of bytes TO BE rendered into the payload of a CSRMech packet.
}

@defthing[Packet bytes? #:value (packet bytes?)]{
A @racket[(define-new-subtype Packet (packet Bytes))] that represents a ready to send CSRMesh packet of some payload/message.
}

@defthing[SeqNo Word32 #:value (seqno Word32)]{
A @racket[(define-new-subtype Seqno (seqno Word32))] that represents a packet sequence number used in the creation of a CSRMesh packet.
}

The sequence number of a packet is not only essential to the packet encryption but is also used by the mesh network in packet transmission.  The mesh "remembers" recently transmitted packet sequence numbers and will drop packets with the same sequence number.  An application using this library should ensure non-duplication of packet sequence numbers by either a global incrementing counter, a random generation of the next sequence number or similar scheme.

@subsection{CSRMesh Packets}

@defproc[(string->Pin [pin String]) Pin]{
Constructs a @racket[Pin] from an ascii string.  Typically this is a 4 digit ascii value, e.g. "1234".
}

@defproc[(network-key [pin Pin]) NetworkKey]{
Cryptographically calculates the CSRMesh NetworkKey from a given @racket[Pin].  This is deterministic as the same @racket[Pin] always creates the same @racket[NetworkKey].
}

@defproc[(build-packet-with-key [key NetKey][sno SeqNo][payload Payload]) Packet]{
Creates a ready to transmit CSRMesh Packet for the give payload, network key and sequence number.
}

@defproc[(build-packet-with-pin [pin Pin][sno SeqNo][payload Payload]) Packet]{
Creates a ready to transmit CSRMesh Packet for the give payload, pin and sequence number.
}

Re-calculates the @racket[NetworkKey] from the givein @racket[Pin] and then calls @racket[build-packet-with-key].

As a @racket[NetworkKey] is uniquely cryptographically generated from a Pin, an application may generate the Network Key once for a Pin via @racket[network-key] and then using @racket[build-packet-with-key] directly.

@subsection{Example Sketch}

@racketblock[
  (define pin : Pin (string->Pin "1234"))
  (define net-key : NetworkKey (network-key pin))
  (define my-payload : Payload  (payload (bytes #x01 #x02 #x03)))
  (define next-seq-no : SeqNo (seqno (random 1000000)))
  (define csr-pkt : Packet (build-packet-with-key net-key next-seq-no my-payload))
]