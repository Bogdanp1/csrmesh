#lang info
(define collection "csrmesh")
(define deps '("crypto" "bitsyntax" "word" "typed-racket-more" "typed-racket-lib" "base" ))
(define build-deps '("scribble-lib" "typed-racket-doc" "racket-doc" "rackunit-lib"))
(define scribblings '(("scribblings/csrmesh.scrbl" ())))
(define pkg-desc "Implementation of the CSR Mesh protocol.")
(define version "0.1.0")
(define pkg-authors '("Raymond Racine"))

